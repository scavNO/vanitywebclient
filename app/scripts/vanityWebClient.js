'use strict';

var vanityWebClient = angular.module('vanityWebClient',
    ['ngRoute',
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngResource',
        'ngRoute',
        'vanityWebClient.directives',
        'vanityWebClient.blizzardService',
        'vanityWebClient.guildService'
    ]
);

vanityWebClient.run(['$rootScope', '$injector', function($rootScope, $injector) {
    $injector.get("$http").defaults.transformRequest = function(data, headersGetter) {

        headersGetter()['Authorization'] = "Bearer 7408f768-8115-4311-a273-a1815ea22fa9";

        if (data) {
            return angular.toJson(data);
        }
    };
}]);

vanityWebClient.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: 'partials/main.html',
        controller: MainController
    });

    /**
     * Route for the mealLog view.
     */
    $routeProvider.when('/members', {
        templateUrl: 'partials/members.html',
        controller: MembersController
    });

    $routeProvider.otherwise({
        redirectTo: '404.html'
    });
}]);