'use strict';

var MembersController = function($scope,$log, Members) {

    $scope.members = Members.query();
    $log.info($scope.members);
};