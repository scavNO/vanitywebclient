'use strict';

var blizzardService = angular.module('vanityWebClient.blizzardService', ['ngResource']);

var apiUrl = 'https://eu.api.battle.net/wow/guild/frostwhisper/Vanity?';

blizzardService.factory('Members', ['$resource',
    function($resource) {
        return $resource(apiUrl, {}, {
            query:{
                method:'JSONP',
                params:{
                    jsonp:'JSON_CALLBACK',
                    fields:'members',
                    locale:'en_GB',
                    apiKey:'w6ueptracqw4kjbf2ux3fgcw96qzac54'
                }
            }
        });
    }
]);

blizzardService.factory('News', ['$resource',
    function($resource) {
        return $resource(apiUrl, {}, {
            query:{
                method:'JSONP',
                params:{
                    jsonp:'JSON_CALLBACK',
                    fields:'news',
                    locale:'en_GB',
                    apiKey:'w6ueptracqw4kjbf2ux3fgcw96qzac54'
                }
            }
        });
    }
]);